FROM ubuntu:18.04

WORKDIR /app

ENV TZ=Europe/Berlin
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
RUN apt update
RUN apt install -y git build-essential wget iproute2 ifupdown isc-dhcp-server libfontconfig1 libsm6 libxrender1 net-tools

# Tools for running, building and debugging inside container
RUN apt install -y vim rsyslog bear make netcat
RUN service rsyslog start

RUN mkdir /vimba
COPY .VIMBA-TARGET /vimba/sdk.tgz
RUN tar -xzf /vimba/sdk.tgz -C /vimba
RUN bash /vimba/*/VimbaGigETL/Install.sh
RUN ln -s /vimba/*/VimbaC /usr/include/VimbaC
RUN cp /vimba/*/VimbaC/DynamicLib/$(ls /vimba/*/VimbaC/DynamicLib/ | grep $(uname -m))/libVimbaC.so /usr/lib
RUN cp /vimba/*/VimbaImageTransform/DynamicLib/$(ls /vimba/*/VimbaImageTransform/DynamicLib/ | grep $(uname -m))/libVimbaImageTransform.so /usr/lib

COPY utils /vimba

ENTRYPOINT [ "/bin/bash", "--rcfile", "/etc/profile" ]
