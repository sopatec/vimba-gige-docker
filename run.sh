#!/bin/bash

volstr=""
if [ $# -eq 1 ]; then
    volstr="-v $1:/app"
fi

xhost +local:docker
docker run -it --cap-add NET_ADMIN -e DISPLAY -v /tmp/.X11-unix:/tmp/.X11-unix --ipc=host --net=host $volstr vimba-testenv
