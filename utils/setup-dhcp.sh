#!/bin/bash

if [ $# -ne 1 ]; then
    echo "USAGE: setup-net.sh <interface>"
    exit 1
fi

service isc-dhcp-server stop

echo "
source-directory /etc/network/interfaces.d

allow-hotplug $1
iface $1 inet static
    address 192.168.0.1/24
    gateway 192.168.0.1
    netmask 255.255.255.0
" > /etc/network/interfaces

echo "
option domain-name \"testnet.org\";
option domain-name-servers 8.8.8.8, 1.1.1.1;

default-lease-time 3600;
max-lease-time 7200;

authoritative;

subnet 192.168.0.0 netmask 255.255.255.0 {
    option routers              192.168.0.1;
    option subnet-mask          255.255.255.0;
    option domain-search        \"testnet.lan\";
    option domain-name-servers  192.168.0.1;
    range   192.168.0.10   192.168.0.100;
}
" > /etc/dhcp/dhcpd.conf

echo "
INTERFACESv4=\"$1\"
INTERFACESv6=\"\"
" > /etc/default/isc-dhcp-server

ip addr flush dev $1
service networking restart

service isc-dhcp-server start
