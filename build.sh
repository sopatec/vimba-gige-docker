#!/bin/bash

REPOROOT=$(git rev-parse --show-toplevel)

if [ $# -ne 1 ]; then
    echo "USAGE: build.sh <PATH-TO-SDK>"
    exit 1
fi

if [ ! -e "$1" ]; then
    echo "Specified path does not exist!"
    exit 2
fi

ln -f "$(realpath $1)" "$REPOROOT/.VIMBA-TARGET"

docker build -t vimba-testenv -f "$REPOROOT/Dockerfile" "$REPOROOT"
